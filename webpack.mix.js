const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .js("resources/js/main.js", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .sass("resources/sass/main.scss", "public/css")
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/json', 'public/json')
    .browserSync("laratogel.dev")
    .version()

    .disableNotifications()


    /* Options */
    .options({
        processCssUrls: false,
        watchOptions: {
            ignored: /node_modules/
        }
    });

mix.autoload({
    'jquery': ['$', 'window.jQuery', 'jQuery'],
    'vue': ['Vue', 'window.Vue'],
    'moment': ['moment', 'window.moment'],
})


mix.webpackConfig({
    output: {
        chunkFilename: 'public/js/chunks/[name].js',//replace with your path
    },
});
