@extends('layouts.app')


@section('content')
@includeIf('keluaran.mobileOverlay')

<div class="center-container">
  <div class="main-container">
    <div class="nav">
      <div>
        <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
            style="padding:10px" /></a>
      </div>
      <div class="navbar" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
        <ul class="top-nav">

          @includeIf('menu.pc')

          {{-- {!! buildMenu() !!}
            @includeIf('menu.pc') --}}

        </ul>
        <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}" class="mobile-menu-icon open-mobilenav" />
      </div>


    </div>


    @includeIf('partial.headerbanner')



    <div class="results-container">
      <h1 class="center">{{ option('lombaTogelTitle') }}</h1>
      <h2 class="center">{{ getCurrentPeriod() }}</h2>
      <br />


      {{-- check if user authenticated --}}
      @if (!Auth::guest() && Auth::user()->level === 'user')
      @includeIf('tebakskor.welcomeUser')
      @includeIf('tebakskor.snk')

      @includeIf('tebakskor.isiprediksi')
      @includeIf('tebakskor.klasemen')

      @else

      {{-- return login screen for unauthenticated user --}}
      @includeIf('tebakskor.login')
      @endif




    </div>

    <br />

    @includeIf('partial.share')



    <br />
    {{-- <div style="width: 100%; height: 2px"><img src="/images/border-sep.png" style="width: 100%" height="2" />
        </div> --}}
    <br />
    <br />



  </div>
</div>


@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')




<div class="footer-container">


</div>

@includeif('partial.footerbanner')
@includeIf('keluaran.footerModal')

@endsection
