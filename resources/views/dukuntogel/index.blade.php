@extends('layouts.app')


@section('content')
@includeIf('keluaran.mobileOverlay')

<div class="center-container">
    <div class="main-container">
        <div class="nav">
            <div>
                <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
                        style="padding:10px" /></a>
            </div>
            <div class="navbar" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                <ul class="top-nav">

                    @includeIf('menu.pc')

                    {{-- {!! buildMenu() !!}
            @includeIf('menu.pc') --}}

                </ul>
                <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}"
                    class="mobile-menu-icon open-mobilenav" />
            </div>


        </div>

        @includeIf('partial.headerbanner')
        <div class="article-container">


            <article>
                <div class="content">
                    <h1 class="center">{{ option('dukunTogelTitle') }}</h1>
                    <h2 class="center">{{ getDukunTogelCurrentPeriode() }}</h2>

                    {{-- check if user authenticated --}}
                    @if (!Auth::guest() && Auth::user()->level === 'user')
                    @includeIf('tebakskor.welcomeUser')

                    <div class="spacer"></div>
                    <syarat-dan-ketentuan-dukun></syarat-dan-ketentuan-dukun>
                    <div class="spacer"></div>


                    @includeIf('dukuntogel.isiprediksi')
                    {{-- @includeIf('tebakskor.klasemen') --}}

                    @else

                    {{-- return login screen for unauthenticated user --}}
                    <syarat-dan-ketentuan-dukun></syarat-dan-ketentuan-dukun>
                    <div class="spacer"></div>


                    @includeIf('tebakskor.login')
                    <div class="spacer"></div>

                    <div class="spacer"></div>
                    <klasemen-dukun></klasemen-dukun>
                    <div class="kata-pengantar">

                        {!! loadPageBySlug('kata-pengantar') !!}
                    </div>
                    @endif




                </div>
            </article>


            @includeIf('partial.sidebar')

            @includeIf('partial.share')
        </div>


    </div>
</div>



@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')

<div class="footer-container">


</div>

@includeIf('keluaran.footerModal')

@endsection
