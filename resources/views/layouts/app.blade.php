<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {!! buildTitle(Route::currentRouteName()) !!}

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-site-verification" content="{{ option('google-site-verification') }}" />

    {{-- <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet"> --}}
    <link rel="stylesheet"
        href="{{ (Route::currentRouteName() !=='dashboard') ? mix('css/main.css') : mix('css/app.css')  }}">

    <script src="{{ (Route::currentRouteName() !=='dashboard') ? mix('js/main.js') : mix('js/app.js') }}" defer>
    </script>


    @if(Route::currentRouteName() !== 'dashboard')

    {!! loadCustomCss() !!}

    @endif


</head>


<body>

    <div id="app">
        <vue-confirm-dialog></vue-confirm-dialog>
        @yield('content')
        @if (Route::currentRouteName() !=='dashboard')

        @includeIf('partial.popup')
        @endif
    </div>
    @yield('contentx')


    {!! loadCustomJs() !!}

    <script>
        $csrfToken="{{csrf_token()}}"
    </script>
</body>

</html>
