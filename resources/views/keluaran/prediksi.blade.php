@extends('layouts.app')
@section('content')
@includeIf('keluaran.mobileOverlay')
<div class="center-container">
  <div class="main-container">
    <div class="nav">
      <div>
        <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
            style="padding:10px" /></a>
      </div>
      <div class="navbar">
        <ul class="top-nav">

          {{-- {!! buildMenu() !!} --}}
          @includeIf('menu.pc')

        </ul>
        <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}" class="mobile-menu-icon open-mobilenav" />
      </div>


    </div>
    @includeIf('partial.headerbanner')
    <div class="article-container">
      <article>

        {{-- <img itemprop="image" src="/images/prediksi_bg/prediction_bg_hongkong.jpg" /> --}}
        @isset($data->provider->banner)
        <div class="prediksi-banner">
          <img src="{{ $data->provider->banner }}" alt="" style="width:100%;">
        </div>
        @endisset

        <div class="content prediksi">
          <div class="top3bandar">
            @includeIf('partial.top3bandar')
          </div>
          <div class="bandartogel-button-area">
            <a href="{{ route('gotoPage',loadPageSlug(option('idPageBandarTogel'))) }}" class="button-bandartogel"
              rel="noopener" target="_blank">{{ option('textTombolBandarTogel') }}</a>
          </div>
          <div class="statistik-banner">
            {!! loadBanner('statistik') !!}
          </div>



          <h2 class="single-title-prediksi">
            {{  implode(' ', [sprintf(option('titlePagePrediksiPerProvider'), ucwords($provider)), ucwords($hari), $tanggal]) }}
          </h2>
          <br><br>
          Lagi bingung mau pasang taruhan apa untuk Togel {{ucwords( $provider) }} hari ini? Selalu cek prediksi dari
          tim
          {{ option('websiteName')}} <br>
          <br>
          Semua pemain togel pastinya sering mencari bahan atau sumber prediksi yang jitu dan akurat sebagai dasar
          pertimbangan untuk taruhan togel. Prediksi yang kami sediakan cocok untuk semua varian permainan togel
          <Pasaran> seperti prediksi 2D, 3D ,4D, Colok Bebas hingga Shio.
            <br>
            <br>
            Nah langsung saja cek prediksi {{ucwords( $provider) }} periode berikutnya:
            <br>

            <br>
            <p><span class="title-prediksi">Angka Main</span> : <strong>{{  $data->angka_main }}</strong></p>
            <br>
            <p><span class="title-prediksi">Shio</span> : <strong> {{ $data->prediksi_shio }}</strong></p>
            <br>
            <p><span class="title-prediksi">Dasar</span> : <strong> {{ $data->prediksi_dasar }}</strong></p>
            <br>
            <p><span class="title-prediksi">Ekor</span> : <strong> {{ $data->prediksi_ekor }}</strong></p>
            <br>
            <p><span class="title-prediksi">Colok Bebas</span> : <strong>
                {{ $data->prediksi_colok_bebas }}</strong>
            </p>
            <br>
            <p><span class="title-prediksi">Colok Bebas Macau</span> : <strong>
                {{ $data->prediksi_colok_macau}}</strong></p>
            <br>
            <p><span class="title-prediksi">Bolak Balik</span> : <strong>
                {{ $data->prediksi_anjuran_bolak_balik}}</strong></p>
            <br>



            <p><span class="title-prediksi">BBFS 2D</span> : <strong>{{ $data->prediksi_2d}}</strong></p>
            <br>
            <p><span class="title-prediksi">BBFS 3D</span> : <strong>{{ $data->prediksi_3d}}</strong></p>
            <br>
            <p><span class="title-prediksi">BBFS4D</span> : <strong>{{ $data->prediksi_4d}}</strong></p>
            <br>

            Diatas adalah prediksi yang sudah kami sediakan dan kami anjurkan untuk dipasang, tapi kami tegaskan
            sekali lagi semua prediksi tidak ada yang terjamin 100%, prediksi hanya sebagai dasar pertimbangan dan
            anjuran untuk taruhan judi togel online. Jadi jangan lupa untuk prioritaskan prediksi dan pilihan
            utama kalian sendiri ya guys!
            <br>
            <br>
            Selain prediksi diatas, kalian juga bisa melakukan analisa melalui rangkuman data singkat yang sudah
            kami rangkum dibawah ini:
            <br>
            <br>
            <h3>Data Keluaran {{ucwords( $provider) }} 30 Undian Terakhir</h3>

            <br>
            <table align="center" class="table-prediksi-askop table-bordered responsive-table" border="1"
              style="width:100%;height:100%;">
              <tr>
                <th>Posisi</th>
                <th colspan="2">Majoritas</th>
                <th>Sering Keluar</th>
                <th>Jarang Keluar</th>
              </tr>
              @foreach ($data->prediksi_askop as $key => $item)
              <tr>
                <td>{{ ucwords($key) }}</td>
                <td>{{ $item->kiri }}</td>
                <td>{{ $item->kanan }}</td>
                <td>{{ $item->seringg }}</td>
                <td>{{ $item->jarang }}</td>

              </tr>
              @endforeach

            </table>
            <br>
            <br>

            <!-- START POLA2D -->

            <h3> Pola 2D Keluaran {{ucwords( $provider) }} 10 Undian Terakhir</h3><br>


            {{-- <table align="center" class="table-prediksi-askop table-bordered responsive-table" border="1"
            style="width:100%;height:100%;"> --}}
            <table class="shio-table">

              @foreach ($data->prediksi_pola2d as $key => $item)
              <tr>
                <td class="head">{{ ucwords($key) }}</td>
                @foreach ($item as $item)
                <td class="{{ strtolower($item) }}">{{ $item }}</td>

                @endforeach


              </tr>
              @endforeach

            </table>
            <br>
            <br>
            <!-- END POLA2D -->


            Sekian informasi prediksi togel {{ ucwords( $provider) }} yang kami sediakan hari ini.
            <br>
            <br>
            Untuk informasi data statistik togel {{ucwords( $provider) }} yang lebih lengkap dan akurat bisa cek disini
            :
            <a href="{{ route('datakeluaran',$provider) }}"> Keluaran Togel {{ucwords( $provider) }} Hari Ini</a>

            <br>
            <br>
            Salam {{ option('websiteName') }}!
            <br>
            <br>
            <hr>
            5 Prediksi Terakhir Pasaran Togel {{ ucwords($provider) }}:

            <ul class="yesterday-result">
              @if ($data->periode_sebelumnya5)
              @foreach ($data->periode_sebelumnya5 as $item)
              <li><a
                  href="{{ route('prediksi.tomorrow',[$provider,strtolower($item->hari),implode('-',array_reverse(explode('-',$item->tanggal)))]) }}">Prediksi
                  Togel {{ ucwords($provider) }}
                  ({{ $data->provider->kode }}),
                  {{ ucwords($item->hari) }} {{ implode('-',array_reverse(explode('-',$item->tanggal))) }} -
                  {{  option('websiteName') }}</a></li>
              @endforeach

              @endif



            </ul>

            <div class="join-telegramgroup-wrapper">
              <div class="join-telegramgroup-chatbubbles">
                <div class="join-telegramgroup-logo">
                  <div class="join-telegramgroup">
                    <span>{{ option('joinGroupText') }}</span>
                    <a href="{{ option('joinGroupUrl') }}" class="button inline yellow"
                      target="_blank">{{ option('joinGroupButtonText') }}</a>
                  </div>
                </div>
              </div>
            </div>


        </div>
      </article>
      @includeIf('partial.sidebar')
    </div>

    <br />
    {{-- <img src="{{ asset('/images/border-sep.png') }}" alt="border separator" width="100%" height="2" /> --}}


  </div>
</div>
@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')
<div class="footer-container">

</div>

<div id="lucky-modal" class="how-lucky-modal">
  <a href="" class="close-lucky toggle-modal-lucky"><img alt="close nav"
      src="{{ asset('/images/close-nav2.svg') }}" /></a>
  <div class="howlucky-container">
    <div class="left"><img alt="lucky girl" src="https://cdn.telejudi.com/rh/lucky_girl.png" /></div>
    <div class="right">

      <h1>Seberapa Beruntung Anda?</h1>
      <h4>Cek nomor anda berdasarkan ratusan keluaran.</h4>
      <form id="lucky-form">
        <label for="lucky-input">Angka 4D</label>
        <input pattern="[0-9]*" inputmode="numeric" type="number" id="lucky-input" maxlength="4" placeholder="1536"
          class="howluckyinput" />
        <br />
        <input type="submit" value="Cek!" class="howluckysubmit" />
      </form>
    </div>
  </div>
</div>

@includeif('partial.footerbanner')
@includeIf('keluaran.footerModal')

<script type="application/ld+json">

</script>
<script type="application/ld+json">

</script>

@endsection
