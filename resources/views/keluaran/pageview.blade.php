@extends('layouts.app')


@section('content')
@includeIf('keluaran.mobileOverlay')

<div class="center-container">
    <div class="main-container">
        <div class="nav">
            <div>
                <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
                        style="padding:10px" /></a>
            </div>
            <div class="navbar" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                <ul class="top-nav">

                    @includeIf('menu.pc')

                    {{-- {!! buildMenu() !!}
            @includeIf('menu.pc') --}}

                </ul>
                <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}"
                    class="mobile-menu-icon open-mobilenav" />
            </div>


        </div>

        @includeIf('partial.headerbanner')
        <div class="article-container">
            <article>


                <div class="content">
                    {!! $data->html !!}


                    @includeIf('partial.share')



                </div>

            </article>
            @includeIf('partial.sidebar')


        </div>


    </div>
</div>



@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')

<div class="footer-container">


</div>

@includeIf('keluaran.footerModal')

@endsection
