@extends('layouts.app')


@section('content')
@includeIf('keluaran.mobileOverlay')

<div class="center-container">
  <div class="main-container">
    <div class="nav">
      <div>
        <a href="/"><img alt="{{ option('websiteName') }} logo" src="{{ option('websiteLogo') }}"
            style="padding:10px" /></a>
      </div>
      <div class="navbar" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
        <ul class="top-nav">

          @includeIf('menu.pc')

          {{-- {!! buildMenu() !!}
            @includeIf('menu.pc') --}}

        </ul>
        <img alt="close menu" src="{{ asset('/images/menu-nav.svg') }}" class="mobile-menu-icon open-mobilenav" />
      </div>


    </div>


    @includeIf('partial.headerbanner')



    <div class="results-container">
      <h1 class="center">
        {{ option('titlePagePrediksiHariIni')  }}</h1>
      <br />
      <div class="results-grid">


        @foreach ($dataKeluaran as $key => $item)
        @if (option('limitHomeItem') !== '0' && $key < option('limitHomeItem') && count($item->datakeluaran) > 0
          )



          <!-- RESULT ITEM -->
          <div class="results-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
            <a
              href="{{  route('prediksi.tomorrow',detectHariEsokSpecial(strtolower($item->nama),$item->datakeluaran[0]->idTanggal)) }}">
              <div class="results-country" itemprop="name">
                <img class="lazy" width="60" height="40" src="/images/countryflags/{{ strtolower($item->kode) }}.svg" />
                <span>
                  <span class="none">Keluaran Togel</span>
                  {{ $item->nama }}
                  <span class="none">Hari Ini</span>
                  <span class="none">{{ $item->datakeluaran[0]->hari }},
                    {{ formatDate($item->datakeluaran[0]->idTanggal) }}</span>
                </span>
              </div>

              <div class="results-result lazy"
                style="background-image:url('{{ ($item->gambar !== null) ? $item->gambar : '/images/countryflags/'.strtolower($item->kode).'.svg' }}');">
                <span itemprop="text">{{ todayHariTanggal($item->datakeluaran[0]) }}</span>
              </div>
            </a>
            <div class="results-history">
              <div class="pasaran">

              </div>
              <div class="title">Prediksi Lama</div>
              <div style="width: 100%; height: 2px"><img src="/images/border-sep.png" style="width: 100%" height="2" />
              </div>

              @foreach ($item->datakeluaran as $key => $keluaranLalu)
              @if ($key<4) <a style="display:flex:padding: 5px 0;"
                href="{{ route('prediksi.tomorrow',[strtolower($item->nama),strtolower($keluaranLalu->hari),$keluaranLalu->idTanggal]) }}">
                <div>{{ $keluaranLalu->hari }},
                  {{ $keluaranLalu->idTanggal }}<span
                    class="buttonLihatPrediksi">{{ option('buttonLihatPrediksiText') }}</span>
                </div>
                </a>

                @endif





                @endforeach

                <div class="more"><a class="button buttonLihatStatistikKeluaran"
                    href="{{ route('datakeluaran',strtolower($item->nama)) }}">{{ option('homepageLihatStatistikKeluaran') }}</a>
                </div>
            </div>
          </div>



          @endif

          @endforeach



      </div>
    </div>

    <br />

    @includeIf('partial.share')



    <br />
    {{-- <div style="width: 100%; height: 2px"><img src="/images/border-sep.png" style="width: 100%" height="2" />
        </div> --}}
    <br />
    <br />



  </div>
</div>


@includeIf('partial.bottomnav')
@includeIf('partial.customfooter')




<div class="footer-container">


</div>

@includeif('partial.footerbanner')
@includeIf('keluaran.footerModal')

@endsection
