<div id="lucky-modal" class="how-lucky-modal">
  <a href="" class="close-lucky toggle-modal-lucky"><img alt="close nav" src="/images/close-nav.svg" /></a>
  <div class="howlucky-container">

    <div class="left"><img alt="lucky girl" src="/images/lucky_girl.png" /></div>
    <div class="right">

      <h1>Seberapa Beruntung Anda?</h1>
      <h4>Cek nomor anda berdasarkan ratusan keluaran.</h4>
      <form id="lucky-form">
        <label for="lucky-input">Angka 4D</label>
        <input pattern="[0-9]*" inputmode="numeric" type="number" id="lucky-input" maxlength="4" placeholder="1536"
          class="howluckyinput" />
        <br />
        <input type="submit" value="Cek!" class="howluckysubmit" />
      </form>
    </div>
  </div>
</div>



<div class="stickyfooter">

  <div id="fixedban">
    <div>
      <a id='close-fixedban' onclick='document.getElementById(&apos;fixedban&apos;).style.display = &apos;none&apos;;'
        style='cursor:pointer;'><img alt='close'
          src='https://1.bp.blogspot.com/-bbKkRcwEIaQ/Xpw-8CgNlAI/AAAAAAAAKEs/xh0DL1YI7AkuaWpjvL9t7XeW-SqW7Lh9wCLcBGAsYHQ/s1600/CLOSE.png'
          title='close button' style='vertical-align:middle;' />
      </a>
    </div>
    <div class="banner-floating">
      {!! loadBanner('floating') !!}
    </div>
  </div>


  @if (strtolower(option('enableModalFooter')) == 'true' )
  <a class="toggle-modal-lucky" href="{{ (option('modalTextLink') !=='') ? option('modalTextLink') : '#' }}"
    target="{{ (strtolower(option('modalTargetNewTab')) == 'true') ? '_blank': '_self' }}">
    <div class="lucky-sticky">
      <div>
        <h6>{{ option('modalTextTitle') }} </h6>
      </div>
      <div class="cek">{{ option('modalTextBody') }}</div>
    </div>
  </a>

  @endif
</div>
