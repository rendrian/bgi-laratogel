<div class="bottomnav flex">
    <ul class="bottom-nav">
        <li><a href="{{route('gotoPage','buku-mimpi')}}">Buku Mimpi</a></li>
        <li><a href="{{route('gotoPage','table-shio')}}">Tabel Shio</a></li>
        @if (strtolower(option('enableContactus')) === 'true')

        <li><a href="{{route('gotoPage','contact-us')}}">Contact Us</a></li>
        @endif


    </ul>
</div>
