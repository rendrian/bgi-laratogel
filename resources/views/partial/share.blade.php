@if (strtolower(option('enableHomepageShare')) == 'true')
<div class="article-container" style="grid-template-columns: 1fr;">
    <article>
        <div class="share-bar">
            <h3>{{ option('homepageShareText') }}</h3>
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}" target="_blank">
                <img src="/images/social/facebook-logo.svg" width="20" />
            </a>
            <a href="https://twitter.com/intent/tweet?text=url={{ url()->full() }}" target="_blank">
                <img src="/images/social/twitter-letter-logo.svg" width="20" />
            </a>
            @if (option('whatsappNumber') == '')
            <a><img src="/images/social/whatsapp-logo.svg" width="20" />
            </a>
            @else
            <a href="https://wa.me/{{ option('whatsappNumber') }}" target="_blank"><img
                    src="/images/social/whatsapp-logo.svg" width="20" />
            </a>
            @endif
            <a href="https://telegram.me/share/url?url={{ url()->full() }}" target="_blank"><img
                    src="/images/social/telegram_logo_white.svg" width="20" />
            </a>
        </div>
    </article>
</div>

@endif
