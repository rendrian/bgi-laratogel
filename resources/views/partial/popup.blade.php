@if (loadBanner('popup') !== '')
<div class="hover_bkgr_fricc">
  <span class="helper"></span>
  <div>
    <div class="popupCloseButton">&times;</div>
    {!! loadBanner('popup') !!}
  </div>
</div>

@endif
