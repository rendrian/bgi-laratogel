require("./bootstrap");
// window.Vue = require('vue');

// app.js

// import 'quill/dist/quill.core.css'
// import 'quill/dist/quill.snow.css'
// import 'quill/dist/quill.bubble.css'

import Vue from "vue";

import VueConfirmDialog from "vue-confirm-dialog";
Vue.use(VueConfirmDialog);
Vue.component("vue-confirm-dialog", VueConfirmDialog.default);

import {
    BootstrapVue,
    BIcon,
    BIconArrowLeft,
    BIconCheckCircle,
    BIconPlusCircle,
    BIconPencilSquare,
    BIconTrash,
    BIconDashCircle,
    BIconUpload
} from "bootstrap-vue";
import VueRouter from "vue-router";

Vue.use(require("vue-moment"));

Vue.component("BIcon", BIcon);
Vue.component("iconArrowLeft", BIconArrowLeft);
Vue.component("iconCheckCircle", BIconCheckCircle);
Vue.component("iconPlusCircle", BIconPlusCircle);
Vue.component("iconPencil", BIconPencilSquare);
Vue.component("iconTrash", BIconTrash);
Vue.component("iconCircle", BIconDashCircle);
Vue.component("iconUpload", BIconUpload);

import Navbar from "./Navbar";

// Install BootstrapVue
Vue.use(BootstrapVue);

Vue.use(VueRouter);
// Vue.use(IconsPlugin)

// Vue.use(ElementUI);

// Vue.use(ElementTiptapPlugin);
Vue.component("editor", require("./components/HtmlEditor").default);
Vue.component("custom-html", require("./components/HtmlCustom").default);

// import components
import router from "./router";

const app = new Vue({
    el: "#app",
    router,
    components: {
        Navbar
    }
});
