import Vue from 'vue'
import Router from 'vue-router'

// admin import
import Dashboard from './components/Dashboard'
import Users from './components/admin/Users'
import UserList from './components/admin/user/List'
import UserEdit from './components/admin/user/Edit'
import Sites from './components/admin/Sites'

import Sidebar from './components/admin/Sidebar'
import SidebarAdd from './components/admin/sidebar/Add'
import SidebarEdit from './components/admin/sidebar/Edit'
import SidebarList from './components/admin/sidebar/List'


// menu
import Menu from './components/admin/Menu'
import MenuAdd from './components/admin/menu/Add'
import MenuList from './components/admin/menu/List'
import MenuEdit from './components/admin/menu/Edit'
// page
import Page from './components/admin/Pages'
import PageList from './components/admin/page/List'
import PageAdd from './components/admin/page/Add'

// custom
import View from './components/admin/View'
import CustomCSS from './components/admin/custom/Css'
import CustomJS from './components/admin/custom/Js'
import CustomMenu from './components/admin/custom/Menu'

import BannerIndex from './components/admin/banner/Index'
import BannerEdit from './components/admin/banner/Edit'


import RekomendasiIndex from './components/admin/rekomendasi/Index'
import RekomendasiEdit from './components/admin/rekomendasi/Edit'

import ManajemenImages from './components/admin/images/Index'


// lomba tebak nomor
import ManajemenPeriode from './components/lomba/Periode'
import ManajemenHasilPeriode from './components/lomba/Hasil'
import TambahPeriode from './components/lomba/Add'
import ManajemenCustomHasil from './components/lomba/Custom'


import DukunPeriode from './components/DukunTogel/Periode'
import DukunHasil from './components/DukunTogel/Hasil'
import DukunCustom from './components/DukunTogel/Custom'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            children: [
                {
                    path: 'images',
                    name: 'manajemenImage',
                    component: ManajemenImages
                },
                {
                    path: 'user',
                    name: 'manajemenUser',
                    component: Users,
                    children: [
                        {
                            path: 'list',
                            name: 'manajemenUserList',
                            component: UserList
                        },
                        {
                            path: 'edit/:id',
                            name: 'manajemenUserEdit',
                            component: UserEdit
                        }
                    ]
                },
                {
                    path: 'site',
                    name: 'manajemenSitus',
                    component: Sites
                },
                {
                    path: 'sidebar',
                    name: 'manajemenSidebar',
                    component: Sidebar,
                    children: [
                        {
                            path: 'add',
                            name: 'manajemenSidebarAdd',
                            component: SidebarAdd
                        },
                        {
                            path: 'list',
                            name: 'manajemenSidebarList',
                            component: SidebarList
                        },
                        {
                            path: 'edit/:id',
                            name: 'manajemenSidebarEdit',
                            component: SidebarEdit
                        }

                    ]
                },
                {
                    path: 'menu',
                    name: 'manajemenMenu',
                    component: Menu,
                    children: [
                        {
                            path: 'list',
                            name: 'manajemenMenuList',
                            component: MenuList
                        },
                        {
                            path: 'add',
                            name: 'manajemenMenuAdd',
                            component: MenuAdd
                        },
                        {
                            path: 'edit/:id',
                            name: 'manajemenMenuEdit',
                            component: MenuEdit
                        }

                    ]
                },
                {
                    path: 'page',
                    name: 'manajemenPage',
                    component: Page,
                    children: [
                        {
                            path: 'list',
                            name: 'manajemenPageList',
                            component: PageList
                        },
                        {
                            path: 'add',
                            name: 'manajemenPageAdd',
                            component: PageAdd
                        },
                        {
                            path: 'edit/:id',
                            name: 'manajemenPageEdit',
                            component: PageAdd
                        },


                    ]
                },
                {
                    path: 'custom',
                    name: 'manajemenCustom',
                    component: View,
                    children: [
                        {
                            path: 'css',
                            name: 'manajemenCustomCSS',
                            component: CustomCSS
                        },
                        {
                            path: 'js',
                            name: 'manajemenCustomJS',
                            component: CustomJS
                        },
                        {
                            path: 'menu',
                            name: 'manajemenCustomMenu',
                            component: CustomMenu
                        },



                    ]
                },
                {
                    path: 'banner',
                    name: 'manajemenBanner',
                    component: View,
                    children: [
                        {
                            path: 'index',
                            name: 'manajemenBannerIndex',
                            component: BannerIndex
                        },
                        {
                            path: 'edit/:position',
                            name: 'manajemenBannerEdit',
                            component: BannerEdit
                        },



                    ]
                },
                {
                    path: 'rekomendasi',
                    // name: 'manajemenBanner',
                    component: View,
                    children: [
                        {
                            path: 'index',
                            name: 'manajemenRekomendasi',
                            component: RekomendasiIndex
                        },
                        {
                            path: 'edit/:uuid?',
                            name: 'manajemenRekomendasiEdit',
                            component: RekomendasiEdit
                        },



                    ]
                },
                {
                    path: 'dukuntogel',
                    // name: 'manajemenBanner',
                    component: View,
                    children: [
                        {
                            path: 'periode/:uuid?',
                            name: 'periodeDukunTogel',
                            component: DukunPeriode
                        },
                        {
                            path: 'hasil',
                            name: 'hasilDukunTogel',
                            component: DukunHasil
                        },
                        {
                            path: 'custom',
                            name: 'customDukunTogel',
                            component: DukunCustom
                        },



                    ]
                },
                {
                    path: 'lomba',
                    // name: 'manajemenBanner',
                    component: View,
                    children: [
                        {
                            path: 'index',
                            name: 'manajemenPeriode',
                            component: ManajemenPeriode
                        },
                        {
                            path: 'hasil',
                            name: 'manajemenHasilPeriode',
                            component: ManajemenHasilPeriode
                        },
                        {
                            path: 'custom',
                            name: 'manajemenCustomHasil',
                            component: ManajemenCustomHasil
                        },
                        {
                            path: ':uuid?/edit',
                            name: 'manajemenTambahPeriode',
                            component: TambahPeriode
                        },



                    ]
                },
            ]
        }
    ]
})
