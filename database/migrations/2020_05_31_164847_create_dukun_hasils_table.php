<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDukunHasilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dukun_hasil', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_tebakan');
            $table->uuid('id_user');

            $table->text('colokbebas_sydney')->nullable();
            $table->text('kombinasi_sydney')->nullable();
            $table->integer('skor_sydney');

            $table->text('colokbebas_hongkong')->nullable();
            $table->text('kombinasi_hongkong')->nullable();
            $table->integer('skor_hongkong');

            $table->integer('totalskor');
            $table->integer('keluar');
            $table->date('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dukun_hasil');
    }
}
