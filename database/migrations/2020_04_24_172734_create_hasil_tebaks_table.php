<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHasilTebaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lomba_hasil', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_tebakan');
            $table->uuid('id_periode');
            $table->uuid('id_user');
            $table->date('tanggal');
            $table->string('penilaian');
            $table->integer('poin');
            $table->string('angka_keluar');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lomba_hasil');
    }
}
