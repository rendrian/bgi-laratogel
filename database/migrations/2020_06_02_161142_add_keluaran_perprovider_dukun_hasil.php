<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeluaranPerproviderDukunHasil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dukun_hasil', function (Blueprint $table) {
            $table->integer('keluaran_sydney')->nullable()->after('totalskor');
            $table->integer('keluaran_hongkong')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dukun_hasil', function (Blueprint $table) {
            $table->dropColumn('keluar');

        });
    }
}
