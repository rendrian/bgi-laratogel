<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodeTebak extends Model
{
    protected $table = 'lomba_periode';
    protected $fillable = ['id', 'nama', 'mulai', 'selesai', 'keterangan', 'status'];
    protected $keyType = 'string';
    public $incrementing = false;
    protected $casts = [
        'status' => 'boolean',
        'id' => 'string'
    ];


    public function userInput()
    {
        return $this->hasMany('App\TebakanUser', 'id_periode', 'id')->orderBy('tanggal','DESC');
    }
}
