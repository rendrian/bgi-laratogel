<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';


    protected $fillable = [
        'name', 'type', 'value', 'position'
    ];
    protected $hidden = [
        'updated_at', 'created_at',
    ];
}
