<?php

namespace App;

use App\Console\Commands\hitunghasil;
use HitunganHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class DukunTebakan extends Model
{
    protected $table = 'dukun_tebakan';
    protected $fillable = [
        'id',
        'user_id',
        'periode_id',
        'tebakan',
        'tanggal',
        'status',
        'keterangan',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    protected $casts = [

        'id' => 'string',
        'tebakan' => 'array'
    ];
    protected $appends = ['username'];

    public function getUsernameAttribute()
    {
        $data =  User::where('id', $this->user_id)->first();
        if ($data !== null) {
            return $data->name;
        } else {
            return 'User Notfound';
        }
    }


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    protected static function booted()
    {
        static::saved(function ($tebakan) {

            $colokBebas = $tebakan['colokbebas'];
            $kombinasi2D = $tebakan['kombinasi2d'];

            $kombinasiReal = HitunganHelper::arrayKombonasi2D($kombinasi2D);




            $nilai = [
                'provider' => $provider,
                'keluar' => $angkaKeluar,
                'colokbebas' => $this->hitungSkorColokBebas($tebakanUser['colokbebas'], $angkaKeluar),
                'kombinasi' => $this->hitungSkorKombinasi($this->arrayKombonasi2D($tebakanUser['kombinasi2d']), $angkaKeluar),
            ];

            $nilai['total_skor'] = $nilai['colokbebas']['skor'] + $nilai['kombinasi']['skor'];



            $provider = strtolower($provider);

            // $fillData = [
            //     'id_tebakan' => $tebakan['id'],


            // ];

            // $x = new DukunHasil(['id' => Str::uuid()]);
            // $x->fill($fillData);
            // $x->save();

            // return $tebakan;
            $this->info('Input hasil tebakan provider: ' . ucwords($provider));
            $newUUID = Str::uuid();

            $inputData = [
                'id_tebakan' => $tebakan['id'],
                'tanggal' => $tanggal,
                'id' => $newUUID,
                'keluaran_' . $provider => str_pad($angkaKeluar, 4, "0", STR_PAD_LEFT),
                'id_user' => $tebakan['user_id'],
                'colokbebas_' . $provider => $nilai['colokbebas'],
                'kombinasi_' . $provider => $nilai['kombinasi'],
                'skor_' . $provider => $nilai['total_skor'],

            ];

            $x = new DukunHasil(['id' => Str::uuid()]);
            $x->fill([
                'id_tebakan' => $tebakan->id,
                'id_user' => $tebakan->user_id,
                'colokbebas_sydney' => '',
                'kombinasi_sydney' => '',
                'colokbebas_hongkong' => '',
                'kombinasi_hongkong' => '',
                'tanggal' => $tebakan->tanggal,




            ]);
        });
    }
}



// $table->uuid('id')->primary();
// $table->uuid('user_id');
// $table->uuid('periode_id');
// $table->text('tebakan');
// $table->date('tanggal');
// $table->text('status')->nullable();
// $table->text('keterangan')->nullable();
