<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DukunHasil extends Model
{
    protected $table = 'dukun_hasil';
    protected $fillable = [
        'id',
        'id_tebakan',
        'id_user',
        // sydney
        'colokbebas_sydney',
        'kombinasi_sydney',
        'skor_sydney',
        // hongkong
        'colokbebas_hongkong',
        'kombinasi_hongkong',
        'skor_hongkong',
        'totalskor',
        'tanggal',
        'keluaran_sydney',
        'keluaran_hongkong',
        'provider'
    ];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $casts = [

        'colokbebas_hongkong' => 'json',
        'kombinasi_hongkong' => 'json',

        'colokbebas_sydney' => 'json',
        'kombinasi_sydney' => 'json',
        'keluaran_sydney' => 'string',
        'keluaran_hongkong' => 'string',

    ];

    public function tebakan()
    {
        return $this->hasOne('App\DukunTebakan', 'id', 'id_tebakan');
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
