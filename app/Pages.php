<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'title', 'html', 'featured_image', 'tags', 'slug','description'
    ];
    protected $hidden = [
        'updated_at', 'created_at',
    ];
    protected $casts = [
        'tags' => 'array',
    ];
}
