<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\UserLog;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    public function redirectTo()
    {
        // $MAC = exec('getmac');

        // Storing 'getmac' value in $MAC
        // $MAC = strtok($MAC, ' ');

        $MAC = '00:00:00:00:00:00';
        // dd(url()->previous());
        $user = Auth::user();
        $level =  $user->level;

        if ($user->status === 'inactive') {
            Auth::logout();

            return url()->previous();

        }
        switch ($level) {
            case 'user':
                // return '/' . option('tebakSkorPermalink');
                // Log::info('User Login : ' . $user->name . ' from: ' .  $_SERVER['REMOTE_ADDR'] . ' MAC : ' . $MAC . ' using :' . url()->previous());
                $log = new UserLog(['id' => Str::uuid()]);
                $log->fill([
                    'user_id' => $user->id,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'mac_address' => $MAC,
                    'link' => url()->previous(),
                    'tanggal' => Carbon::today()
                ]);
                $log->save();

                return url()->previous();
                break;
            case 'admin':
                return '/dashboard';
                break;

            default:
                # code...
                break;
        }
        // dd(Auth::user()->level);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }
}
