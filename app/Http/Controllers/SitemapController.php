<?php

namespace App\Http\Controllers;

use App\Pages;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SitemapController extends Controller
{
    public function index()

    {
        $customPageSitemap = [];
        $customSitemap = option('customSitemapArray');
        if (Str::contains($customSitemap, ',')) {
            $customPageSitemap = explode(',', $customSitemap);
        }


        $pageFaq  = Pages::where('slug', 'like', 'faq-%')->get();
        $pageLivedraw  = Pages::where('slug', 'like', 'livedraw-%')->get();

        $keluaran = $this->allKeluaran();



        $content = view('sitemap.index', [
            'keluaran' => $keluaran,
            'faq' => $pageFaq,
            'livedraw' => $pageLivedraw,
            'customsitemap' => $customPageSitemap
        ]);



        return response($content, 200)
            ->header('Content-Type', 'text/xml');
    }

    private function allKeluaran()
    {

        // if (env('APP_ENV') !== 'local') {
        if (option('status') === 'local') {

            return json_decode(@file_get_contents(public_path() . '/json/all.json'), false);
        } else {
            return json_decode(@file_get_contents(option('endPointKeluaranAll') . '?secret=' . base64_encode(env('APP_URL'))), false);
            // return json_decode(@file_get_contents(option('endPointKeluaranAll') . '?secret=' . base64_encode(env('APP_URL'))), false);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }
}
