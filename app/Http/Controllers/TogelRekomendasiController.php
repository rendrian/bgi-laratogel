<?php

namespace App\Http\Controllers;

use App\TogelRekomendasi;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TogelRekomendasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {
            $data = TogelRekomendasi::orderBy('position', 'ASC')->get();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'kode' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = ($request->id !== 'new' ? TogelRekomendasi::findOrFail($request->id) : new TogelRekomendasi(['id' => Str::uuid()]));

            $data->fill([
                "nama" => trim($request->nama),
                "deskripsi" => $request->deskripsi,
                "min_depo" => $request->min_depo,
                "rating" => $request->rating,
                "link" => $request->link
            ]);

            $data->save();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'kode' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TogelRekomendasi  $togelRekomendasi
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        try {
            $data = TogelRekomendasi::findOrFail($uuid);

            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {

            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage(),

                ]
            ], 201);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TogelRekomendasi  $togelRekomendasi
     * @return \Illuminate\Http\Response
     */
    public function edit(TogelRekomendasi $togelRekomendasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TogelRekomendasi  $togelRekomendasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TogelRekomendasi $togelRekomendasi)
    {
        try {
            $x = 1;

            foreach ($request->all() as $req) {
                // return $req;
                TogelRekomendasi::where('id', $req['id'])
                    ->update(['position' => $x]);
                $x++;
            }

            return response([
                'success' => true,
                'msg' => 'Togel Rekomendasi position updated'
            ]);
        } catch (\Throwable $th) {

            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage(),

                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TogelRekomendasi  $togelRekomendasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(TogelRekomendasi $togelRekomendasi, $id)
    {
        try {
            $dataUser =  TogelRekomendasi::where('id', $id)->delete();
            return response([
                'success' => true,
                'msg' => 'success delete rekomendasi'

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage(),

                ]

            ], 201);
        }
    }
}
