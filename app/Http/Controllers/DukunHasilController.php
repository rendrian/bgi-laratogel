<?php

namespace App\Http\Controllers;

use App\DukunHasil;
use App\DukunPeriode;
use App\DukunTebakan;
use App\Pages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class DukunHasilController extends Controller
{



    public function klasemen(Request $request, $userid = '')
    {
        $currentPeriode  = DukunPeriode::orderBy('created_at', 'desc')->first();
        // return $currentPeriode;

        $limit = -1;
        if (isset($request->limit)) {
            $limit = $request->limit;
            // $array = Cache::remember('klasemendukun-currentperiode-with-limit', 60, function () use ($currentPeriode, $limit) {


            //     return DB::table('dukun_hasil')

            //         ->select('dukun_tebakan.periode_id', 'dukun_hasil.id_user', 'users.name', DB::raw('SUM(totalskor) as skor'))

            //         ->join('users', 'dukun_hasil.id_user', 'users.id')
            //         ->join('dukun_tebakan', 'dukun_hasil.id_tebakan', 'dukun_tebakan.id')
            //         ->where('dukun_tebakan.periode_id', $currentPeriode->id)
            //         ->groupBy('id_user')
            //         ->orderBy('skor', 'DESC')
            //         ->orderBy('users.name', 'ASC')
            //         ->limit($limit)
            //         ->get();
            // });
            $array = DB::table('dukun_hasil')

                ->select('dukun_tebakan.periode_id', 'dukun_hasil.id_user', 'users.name', DB::raw('SUM(totalskor) as skor'))

                ->join('users', 'dukun_hasil.id_user', 'users.id')
                ->join('dukun_tebakan', 'dukun_hasil.id_tebakan', 'dukun_tebakan.id')
                ->where('dukun_tebakan.periode_id', $currentPeriode->id)
                ->groupBy('id_user')
                ->orderBy('skor', 'DESC')
                ->orderBy('users.name', 'ASC')
                ->limit($limit)
                ->get();
        } else {
            // $array = Cache::remember('klasemendukun-currentperiode-no-limit', 60, function () use ($currentPeriode, $limit) {


            //     return DB::table('dukun_hasil')

            //         ->select('dukun_tebakan.periode_id', 'dukun_hasil.id_user', 'users.name', DB::raw('SUM(totalskor) as skor'))

            //         ->join('users', 'dukun_hasil.id_user', 'users.id')
            //         ->join('dukun_tebakan', 'dukun_hasil.id_tebakan', 'dukun_tebakan.id')
            //         ->where('dukun_tebakan.periode_id', $currentPeriode->id)
            //         ->groupBy('id_user')
            //         ->orderBy('skor', 'DESC')
            //         ->orderBy('users.name', 'ASC')
            //         ->limit($limit)
            //         ->get();
            // });
            $array = DB::table('dukun_hasil')

                ->select('dukun_tebakan.periode_id', 'dukun_hasil.id_user', 'users.name', DB::raw('SUM(totalskor) as skor'))

                ->join('users', 'dukun_hasil.id_user', 'users.id')
                ->join('dukun_tebakan', 'dukun_hasil.id_tebakan', 'dukun_tebakan.id')
                ->where('dukun_tebakan.periode_id', $currentPeriode->id)
                ->groupBy('id_user')
                ->orderBy('skor', 'DESC')
                ->orderBy('users.name', 'ASC')
                ->limit($limit)
                ->get();
        }



        // return $tebakanDalamPeriode =  DukunTebakan::select('id', 'user_id as')->where('periode_id', $currentPeriode->id)->get();
        // $array = DB::table('dukun_hasil')

        //     ->select('dukun_tebakan.periode_id', 'dukun_hasil.id_user', 'users.name', DB::raw('SUM(totalskor) as skor'))

        //     ->join('users', 'dukun_hasil.id_user', 'users.id')
        //     ->join('dukun_tebakan', 'dukun_hasil.id_tebakan', 'dukun_tebakan.id')
        //     ->where('dukun_tebakan.periode_id', $currentPeriode->id)
        //     ->groupBy('id_user')
        //     ->orderBy('skor', 'DESC')
        //     ->orderBy('users.name', 'ASC')
        //     ->limit($limit)
        //     ->get();






        // return $array = DB::table('dukun_hasil')
        //     ->select('dukun_hasil.id_user', 'users.name', DB::raw('SUM(totalskor) as skor'))

        //     // ->join('dukun_tebakan', 'dukun_hasil.id_tebakan', 'dukun_tebakan.periode_id')
        //     ->join('users', 'dukun_hasil.id_user', 'users.id')
        //     // ->where('dukun_hasil.id_periode', $currentPeriode->id)
        //     ->groupBy('id_user')
        //     ->orderBy('skor', 'DESC')
        //     ->orderBy('users.name', 'ASC')
        //     ->limit($limit)
        //     ->get();


        foreach ($array as $arrax) {

            // $tebakan = Cache::remember('dukun-tebakan', 60, function () use ($arrax) {
            //     return DukunTebakan::where('user_id', $arrax->id_user)->orderBy('tanggal', 'desc')->first();
            // });
            $tebakan =   DukunTebakan::where('user_id', $arrax->id_user)->orderBy('tanggal', 'desc')->first();



            $exist = DukunHasil::where('id_tebakan', $tebakan->id)->first();



            if ($exist === null) {
                $arrax->uncount =  [
                    'id_tebakan' => $tebakan->id,
                    'tanggal' => $tebakan->tanggal,
                    'id' => Str::uuid(),
                    // 'keluaran_' . $provider => str_pad($angkaKeluar, 4, "0", STR_PAD_LEFT),
                    'id_user' => $tebakan->user_id,
                    'colokbebas_sydney'  => [
                        "tebakan" => $tebakan->tebakan['Sydney']['colokbebas'],
                        "skor" => "",
                        "keterangan" => ""
                    ],
                    'kombinasi_sydney' => [
                        "keluar" => "",
                        "tebakan" => $this->cariNilaiKombinasi($tebakan->tebakan['Sydney']['kombinasi2d']),
                        "tebakan_real" => $tebakan->tebakan['Sydney']['kombinasi2d'],
                        "skor" => "",
                        "keterangan" => ""
                    ],
                    'colokbebas_hongkong'  => [
                        "tebakan" => $tebakan->tebakan['Hongkong']['colokbebas'],
                        "skor" => "",
                        "keterangan" => ""
                    ],

                    'kombinasi_hongkong' => [
                        "keluar" => "",
                        "tebakan" => $this->cariNilaiKombinasi($tebakan->tebakan['Hongkong']['kombinasi2d']),
                        "tebakan_real" => $tebakan->tebakan['Hongkong']['kombinasi2d'],

                        "skor" => "",
                        "keterangan" => ""
                    ],


                    // 'skor_' . $provider => $nilai['total_skor'],

                ];
            } else {
                $arrax->uncount = null;
            }



            $arrax->data =  DukunHasil::with('tebakan:id,tebakan')->where('id_user', $arrax->id_user)
                ->where('tanggal', '>=', $currentPeriode->start)
                ->orderBy('tanggal', 'desc')->get();
            // $arrax->data = Cache::remember('tebakan-data', 60, function () use ($arrax, $currentPeriode) {
            //     return DukunHasil::with('tebakan:id,tebakan')->where('id_user', $arrax->id_user)
            //         ->where('tanggal', '>=', $currentPeriode->start)
            //         ->orderBy('tanggal', 'desc')->get();
            // });

            // array_unshift($arrax->data, $datax,);
        }

        return response([
            'success' => true,
            'data' => $array
        ], 200);
    }

    private function detKombinasi($angka)
    {
    }

    private function cariNilaiKombinasi($index)
    {
        $arr = [
            'Besar - Genap',
            'Besar - Ganjil',
            'Kecil - Genap',
            'Kecil - Ganjil',
        ];

        return $arr[$index];
    }

    public function getSNK()
    {
        try {

            $data = Cache::remember('snkdukun', 24, function () {
                return Pages::where('slug', 'snkdukun')->first();
            });
            return response([
                'success' => true,
                'data' => $data->html
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function custommsg()
    {
        $close = Cache::remember('customCloseMessageDukun', 360, function () {
            return  option('customCloseMessageDukun');
        });
        $pengumuman = Cache::remember('key', 360, function () {
            return Pages::where('slug', 'pengumuman-dukun')->first();
        });

        try {
            return response([
                'success' => true,
                'closeMessage' => $close,
                'pengumuman' => $pengumuman,
                'header' => loadPageBySlug('custom-dukun-header'),
                'footer' => loadPageBySlug('custom-dukun-footer')
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function simulasiHapusPeriode()
    {
        $periode =  DukunPeriode::all();

        // [
        //     {
        //     "id": "9ffd5be5-e1b0-4fe0-ac48-216cddeca768",
        //     "name": "periodeterbaru",
        //     "description": "periodeterbari deskripsi",
        //     "start": "2020-05-08",
        //     "end": "2020-05-08"
        //     },
        //     {
        //     "id": "f868e79d-b26e-4528-9c25-422a8e3c2c93",
        //     "name": "perideo terbaru bosku",
        //     "description": "ini deskripsi periode terbaru",
        //     "start": "2020-05-22",
        //     "end": "2020-05-30"
        //     }
        //     ]



        $idDelete = DukunTebakan::where('periode_id', 'f868e79d-b26e-4528-9c25-422a8e3c2c93')->pluck('id');
        $dukunHasilDelete = DukunHasil::whereIn('id_tebakan', $idDelete)->delete();
        return $dukunHasilDelete;
    }
}
