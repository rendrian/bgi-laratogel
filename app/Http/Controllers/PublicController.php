<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Pages;
use App\TogelRekomendasi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(Request $request)
    {
        // $menu = Menu::orderBy('position', 'ASC')->get();
        // return option('limitHomeItem');
        $dataKeluaran =  $this->allKeluaran();
        $customFooter = Pages::where('slug', 'custom-footer')->first();
        $data =  [
            'dataKeluaran' => $dataKeluaran,
            // 'menu' => $menu,
            'customFooter' => $customFooter
        ];

        // return $data;
        if(isset($request->debug)){
            return $data;
        }

        // return $data;
        return view('welcome', $data);
    }
    public function today(Request $request, $provider)
    {
        $provider = explode('-', $provider)[0];
        $datakeluaran =  $this->getData('endPointKeluaranPerProvider', $provider);
        //  dd($datakeluaran);
        if ($datakeluaran->success == true) {
        }

        $slugProvider = str_replace(' ', '-', $provider);
        $faq = Pages::where('slug', 'like',  '%' . strtolower($slugProvider) . '%')->first();
        $about = Pages::where('slug', 'like',  'about-' . strtolower($slugProvider) . '%')->first();
        $customFooter = Pages::where('slug', 'custom-footer')->first();


        $data = [
            'data' =>  $datakeluaran->report,
            'provider' => strtoupper($provider),
            'aboutKeluaran' => $about,
            'faqKeluaran' => $faq,
            'customFooter' => $customFooter,
        ];

        if (isset($request->debug)) {
            return $data;
        }


        // return $data;

        // dd($data);
        // return $data;
        return view('keluaran.today', $data);
        // $dataKeluaran = Togel::loadJson(Helper::endPoint('keluaran') . $provider);
        // // print_r($dataKeluaran);
        // // die;
        // Helper::view('content', [
        //     'data' =>  $dataKeluaran->report,
        //     'provider' => strtoupper($provider)
        // ]);
    }


    public static function viewPageStatic($pagename)
    {
        $page =  Pages::where('slug', $pagename)->first();
        if (!$page) {
            return redirect('/');
        }

        return view('keluaran.pageview', ['data' => $page]);
    }
    public function viewPage($pagename)
    {
        // return $pagename;
        $page =  Pages::where('slug', $pagename)->first();
        if (!$page) {
            return redirect('/');
        }

        return view('keluaran.pageview', ['data' => $page]);
    }

    public function viewRekomendasi()
    {
        $rekomendasi = TogelRekomendasi::orderBy('position', 'ASC')->get();
        return view('keluaran.rekomendasi')->with('TogelRekomendasi', $rekomendasi);
    }

    private function getData($type, $provider)
    {

        // debug
        // return option($type) . $provider;
        return json_decode(@file_get_contents(option($type) . $provider), false);

        // if (option('status') == 'local') {

        //     return json_decode(file_get_contents(public_path() . '/json/allx.json'), false);
        // } else {
        //     return json_decode(@file_get_contents(option($type) . $provider), false);


        //     // 'https://mafiahasil.top/api/data/all.json'
        // }
    }

    private function allKeluaran()
    {

        // if (env('APP_ENV') !== 'local') {
        if (option('status') === 'local') {

            return json_decode(@file_get_contents(public_path() . '/json/all.json'), false);
        } else {
            return json_decode(@file_get_contents(option('endPointKeluaranAll') . '?secret=' . base64_encode(env('APP_URL'))), false);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }
    private function hasilPrediksi($provider, $hari, $tanggal)
    {

        $slug  = $provider . '-' . $hari . '-' . $tanggal;
        $url = option('endPointPrediksi') . $slug;
        // if (env('APP_ENV') !== 'local') {
        if (option('status') === 'local') {

            return json_decode(file_get_contents(public_path() . '/json/prediksi.json'), false);
        } else {
            return json_decode(@file_get_contents($url), false);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }


    public function tomorrow(Request $request, $provider, $hari, $tanggal)
    {
        $formatTanggal =  explode('-', $tanggal);
        $formatTanggal = array_reverse($formatTanggal);
        $formatTanggal = implode('-', $formatTanggal);
        $tanggalSekarang = Carbon::tomorrow();
        $tanggalSekarang = $tanggalSekarang->format('Y-m-d');

        if ($formatTanggal > $tanggalSekarang) {
            return redirect('/');
        }




        // return  [
        //     'tanggalPrediksi' => $formatTanggal,
        //     'tanggalSekarang' =>
        // ];


        $data = $this->hasilPrediksi($provider, $hari, $tanggal);
        if (isset($request->debug)) {
            return [
                'provider' => $provider,
                'hari' => $hari,
                'tanggal' => $tanggal,
                'data' => $data->data,

            ];
        }


        return view('keluaran.prediksi', [
            'provider' => $provider,
            'hari' => $hari,
            'tanggal' => $tanggal,
            'data' => $data->data,

        ]);

        // return response(['data' => $data], 200);
    }

    public function prediksiTogel()
    {
        $dataKeluaran = $this->allKeluaran();
        $customFooter = Pages::where('slug', 'custom-footer')->first();
        $data =  [
            'dataKeluaran' => $dataKeluaran,
            // 'menu' => $menu,
            'customFooter' => $customFooter
        ];

        return view('keluaran.semuaprediksi', $data);
    }
}
