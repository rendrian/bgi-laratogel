<?php

namespace App\Http\Controllers;

use App\DataJackpot;
use Illuminate\Http\Request;

class DataJackpotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataJackpot  $dataJackpot
     * @return \Illuminate\Http\Response
     */
    public function show(DataJackpot $dataJackpot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataJackpot  $dataJackpot
     * @return \Illuminate\Http\Response
     */
    public function edit(DataJackpot $dataJackpot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataJackpot  $dataJackpot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataJackpot $dataJackpot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataJackpot  $dataJackpot
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataJackpot $dataJackpot)
    {
        //
    }
}
