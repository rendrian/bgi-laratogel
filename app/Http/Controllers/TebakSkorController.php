<?php

namespace App\Http\Controllers;

use App\TebakSkor;
use Illuminate\Http\Request;

class TebakSkorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tebakskor.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TebakSkor  $tebakSkor
     * @return \Illuminate\Http\Response
     */
    public function show(TebakSkor $tebakSkor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TebakSkor  $tebakSkor
     * @return \Illuminate\Http\Response
     */
    public function edit(TebakSkor $tebakSkor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TebakSkor  $tebakSkor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TebakSkor $tebakSkor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TebakSkor  $tebakSkor
     * @return \Illuminate\Http\Response
     */
    public function destroy(TebakSkor $tebakSkor)
    {
        //
    }
}
