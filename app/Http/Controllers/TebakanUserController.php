<?php

namespace App\Http\Controllers;

use App\PeriodeTebak;
use App\TebakanUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TebakanUserController extends Controller
{

    public function __construct()
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            die;
        }
        $tanggal =  $request->tanggal;
        $isDone  = TebakanUser::where([
            ['id_user', $user->id],
            ['tanggal', $tanggal],
            ['id_periode', $this->lastPeriode()->id],
        ])->first();
        if (!$isDone) {
            return  response([
                'success' => false,
                'periode' => $this->lastPeriode()
            ], 200);
        }

        $inputData = $isDone->tebakan;
        $tebakan = [

            'tebakan1' => '',
            'tebakan2' => '',
            'tebakan3' => '',
            'tebakan4' => '',
            'tebakan5' => '',
        ];
        foreach ($isDone->tebakan as $key => $value) {
            $x = $key + 1;
            $tebakan['tebakan' . $x] = $value;
        }
        if ($isDone) {
            return response([
                'success' => true,
                'data' => [
                    'user' => $user->name,
                    'periode' => PeriodeTebak::find($isDone->id_periode)->first(),
                    'tebakan' => $tebakan,
                    'void' => ($isDone->void === null ? false : true),
                ]
            ], 200);
        }
        return response(['success' => false], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    private function lastPeriode()
    {

        $lastperiode =  PeriodeTebak::where('status', '1')
            ->orderBy('created_at', 'DESC')->first();

        return $lastperiode;
    }
    public function store(Request $request)
    {
        try {

            // check if user already insert data today
            // $check =TebakanUser::where()
            $user = Auth::user();

            if (!Auth::user()) {
                return response([
                    'status' => false,
                    'error' => [
                        'code' => 'Not Login',
                        'msg' => 'Login First'
                    ]
                ], 201);
            }




            $check  = TebakanUser::where([
                ['id_user', $user->id],
                ['tanggal', $request->tanggal],
                ['id_periode', $this->lastPeriode()->id],
            ])->first();

            if ($check) {
                return response([
                    'status' => false,
                    'error' => [
                        'code' => 'Sudah Diisi',
                        'msg' => 'Anda sudah mengisi tebakan untuk hari ini'
                    ]
                ], 201);
            } else {

                $data  = new TebakanUser([
                    'id' => Str::uuid(),
                    'id_user' => $user->id,
                    'tanggal' => $request->tanggal
                ]);

                $tebakan = [];
                foreach ($request->tebakan as $key => $value) {

                    $tebakan[] .= str_pad($value, 2, '0', STR_PAD_LEFT);
                }
                $data->fill(
                    [
                        'id_periode' => $this->lastPeriode()->id,
                        'tebakan' => array_unique($tebakan),



                    ]
                );

                $data->save();




                return  response([
                    'success' => true,
                    'data' => $data
                ], 200);
            }
        } catch (\Throwable $th) {
            return response([
                'status' => true,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TebakanUser  $tebakanUser
     * @return \Illuminate\Http\Response
     */
    public function show(TebakanUser $tebakanUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TebakanUser  $tebakanUser
     * @return \Illuminate\Http\Response
     */
    public function edit(TebakanUser $tebakanUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TebakanUser  $tebakanUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TebakanUser $tebakanUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TebakanUser  $tebakanUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(TebakanUser $tebakanUser)
    {
        //
    }
}
