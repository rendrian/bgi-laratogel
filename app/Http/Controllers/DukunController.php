<?php

namespace App\Http\Controllers;

use App\DukunPeriode;
use App\DukunTebakan;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DukunController extends Controller
{

    public function status()
    {
        $user = Auth::user();
        $tanggal = date('Y-m-d');
        $periode = DukunPeriode::orderBy('created_at', 'DESC')->first();
        if ($periode ===  null) {
            return response([
                'success' => false,
                'error' => [
                    'code' => 'XX_001',
                    'msg' => 'Tidak ada periode yang sedang berjalan'
                ]
            ], 201);
        }
        $tanggalSekarang = date('Y-m-d');

        $exist = DukunTebakan::where([
            ['user_id', '=',  $user->id],
            ['periode_id', '=', $periode->id],
            ['tanggal',  $tanggalSekarang]
        ])->first();

        if ($exist !== null) {
            return response([
                'success' => true,
                'data' => $exist
            ], 200);
        } else {
            return response([
                'success' => true,
                'data' => null
            ], 200);
        }
    }

    public function tebakanUser()
    {
        try {
            $periode = DukunPeriode::orderBy('created_at', 'DESC')->first();
            if ($periode ===  null) {
                return response([
                    'success' => false,
                    'error' => [
                        'code' => 'XX_001',
                        'msg' => 'Tidak ada periode yang sedang berjalan'
                    ]
                ], 201);
            }

            $tebakan =  DukunTebakan::where('periode_id', $periode->id)
                ->orderBy('created_at', 'DESC')->get();
            return response([
                'success' => true,
                'data' => $tebakan
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => true,
                'data' => null
            ], 200);
        }
    }

    public function user()
    {
        try {

            $custom = [
                'header' => loadPageBySlug('custom-dukun-header'),
                'footer' => loadPageBySlug('custom-dukun-footer')
            ];
            $user = Auth::user();
            return response([
                'success' => true,
                'data' => $user,
                'custom' => $custom

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function add(Request $request)
    {
        try {
            $user = Auth::user();
            $periode = DukunPeriode::orderBy('created_at', 'DESC')->first();
            if ($periode ===  null) {
                return response([
                    'success' => false,
                    'error' => [
                        'code' => 'XX_001',
                        'msg' => 'Tidak ada periode yang sedang berjalan'
                    ]
                ], 201);
            }

            $tanggalSekarang = date('Y-m-d');
            // check if user already input for today
            $exist = DukunTebakan::where([
                ['user_id', '=',  $user->id],
                ['periode_id', '=', $periode->id],
                ['tanggal', '=',  $tanggalSekarang]
            ])->first();
            if ($exist !== null) {
                return response([
                    'success' => false,
                    'error' => [
                        'code' => 'XX_002',
                        'msg' => 'User sudah menginput untuk hari ini'
                    ]
                ], 201);
            }



            $fillData  = [
                'user_id' => $user->id,
                'periode_id' => $periode->id,
                'tebakan' => $request->toArray(),
                'tanggal' => $tanggalSekarang
            ];

            $inputData = new DukunTebakan(['id' => Str::uuid()]);
            $inputData->fill($fillData);
            $inputData->save();

            return response([
                'success' => true,
                'data' => $inputData
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function addPeriode(Request $request)
    {
        try {
            $periode = ($request->uuid === 'new') ? new DukunPeriode(['id' => Str::uuid()]) : DukunPeriode::findOrFail($request->id);
            $periode->fill([
                'name' => $request->name,
                'description' => $request->description,
                'start' => $request->start,
                'end' => $request->end,

            ]);
            $periode->save();
            return response([
                'success' => true,
                'data' => $periode
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function getPeriodeList()
    {
        try {
            $data = DukunPeriode::orderBy('created_at', 'DESC')->get();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function deletePeriodeDetails($uuid)
    {
        try {
            $data = DukunPeriode::findOrFail($uuid);
            $data->delete();
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function getPeriodeDetails($uuid)
    {

        try {
            $data = DukunPeriode::findOrFail($uuid);
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
}
