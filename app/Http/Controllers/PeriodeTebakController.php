<?php

namespace App\Http\Controllers;

use App\HasilTebak;
use App\Pages;
use App\PeriodeTebak;
use App\TebakanUser;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class PeriodeTebakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PeriodeTebak::orderBy('created_at', 'DESC')->get();
        return response([
            'success' => true,
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function result(Request $request)
    {
        // foreach(TebakanUser::all() as $tebak){
        //     return $tebak;
        // }

        $currentPeriode =  PeriodeTebak::with('userInput')
            ->where('status', '1')
            ->orderBy('updated_at', 'DESC')->first();

        return response([
            'success' => true,
            'data' => [
                'periode' => $currentPeriode
            ]
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $periode = $request->id === 'new' ? new PeriodeTebak(['id' => Str::uuid()]) : PeriodeTebak::findOrFail($request->id);
            $data = [
                'nama' => $request->nama,
                'mulai' => $request->mulai,
                'selesai' => $request->selesai,
                'keterangan' => $request->keterangan
            ];

            $periode->fill($data);
            $periode->save();

            return response([
                'success' => true,
                'data' => $periode
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PeriodeTebak  $periodeTebak
     * @return \Illuminate\Http\Response
     */
    public function show(PeriodeTebak $periodeTebak, $periode)
    {
        try {
            $data = $periodeTebak->findOrFail($periode);
            return response([
                'success' => true,
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PeriodeTebak  $periodeTebak
     * @return \Illuminate\Http\Response
     */
    public function edit(PeriodeTebak $periodeTebak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PeriodeTebak  $periodeTebak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PeriodeTebak $periodeTebak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PeriodeTebak  $periodeTebak
     * @return \Illuminate\Http\Response
     */
    public function destroy(PeriodeTebak $periodeTebak, $periode)
    {
        try {
            $delete = $periodeTebak->find($periode)->delete();
            return response([
                'success' => true,

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }


    public function custommsg()
    {
        $close = option('customCloseMessage');
        $pengumuman = Pages::where('slug', 'pengumuman')->first();

        try {
            return response([
                'success' => true,
                'closeMessage' => $close,
                'pengumuman' => $pengumuman,
                'providerName' => option('lombaTogelProviderName'),
                'header' => loadPageBySlug('custom-lomba-header'),
                'footer' => loadPageBySlug('custom-lomba-footer')
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function getSNK()
    {
        try {
            $data = Pages::where('slug', 'snk')->first();
            return response([
                'success' => true,
                'data' => $data->html
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }
    public function getKatapengantar()
    {

        try {
            $data = Pages::where('slug', 'kata-pengantar')->first();
            return response([
                'success' => true,
                'data' => $data->html
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                'error' => [
                    'code' => $th->getCode(),
                    'msg' => $th->getMessage()
                ]
            ], 201);
        }
    }

    public function getKlasemen(Request $request)
    {

        // $faker = Faker::create('id_ID');
        // $array = [];
        // for ($i = 0; $i < 100; $i++) {
        //     array_push($array, [
        //         'id' => Str::uuid(),
        //         'name' => $faker->name(),
        //         'rank' => $i + 1,
        //         'poin' => $faker->randomNumber(3),

        //     ]);
        // }

        // return HasilTebak::all();
        // $array = DB::table('lomba_hasil')
        //     ->select('poin', DB::raw('SUM(poin) as total_poin'))
        //     ->groupBy('total_poin')
        //     ->get();

        $lastperiode =  PeriodeTebak::where('status', '1')
            ->orderBy('created_at', 'DESC')->first();
        $idperiode = ($lastperiode === null ? 'new' : $lastperiode->id);


        if (isset($request->limit)) {
            $array = DB::table('lomba_hasil')
                ->select('users.id', 'users.name', 'lomba_hasil.angka_keluar', 'lomba_hasil.id_user', DB::raw('SUM(poin) as poin'))

                ->join('users', 'lomba_hasil.id_user', 'users.id')
                ->where('lomba_hasil.id_periode', $idperiode)
                ->groupBy('id_user')
                ->orderBy('poin', 'DESC')
                ->orderBy('users.name', 'ASC')
                ->limit(5)
                ->get();
        } else {
            $array = DB::table('lomba_hasil')
                ->select('users.id', 'users.name', 'lomba_hasil.angka_keluar', 'lomba_hasil.id_user', DB::raw('SUM(poin) as poin'))

                ->join('users', 'lomba_hasil.id_user', 'users.id')
                ->where('lomba_hasil.id_periode', $idperiode)
                ->groupBy('id_user')
                ->orderBy('poin', 'DESC')
                ->orderBy('users.name', 'ASC')
                ->get();

            foreach ($array as $arrax) {
                // dd($arrax);
                // return $arrax;
                $arrax->data =  TebakanUser::with('statusTebakan')
                    ->where('id_periode', $idperiode)
                    ->where('id_user', $arrax->id_user)->orderBy('tanggal', 'DESC')->get();
                // $arrax->data = HasilTebak::select('id_tebakan', 'tanggal', 'penilaian', 'poin')->where('id_user', $arrax->id_user)->get();
            }
        }


        // return $array;

        return response([
            'last_perideo' => $lastperiode,
            'success' => true,
            'data' => $array
        ], 200);
    }

    private function getHongkongData()
    {
        if (option('status') === 'local') {

            return json_decode(file_get_contents(public_path() . '/json/hongkong.json'), true);
        } else {
            return json_decode(@file_get_contents(option('endPointKeluaranPerProvider') . 'hongkong'), true);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }

    public function hitungHasil()
    {
        $now = Carbon::today('Asia/Jakarta')->isoFormat('YYYY-MM-DD');
        $yesterday = Carbon::yesterday('Asia/Jakarta')->isoFormat('YYYY-MM-DD');

        // check last data =

        // $data = (object) json_decode(@file_get_contents(option('endPointKeluaranPerProvider') . 'hongkong'), true);
        $data = (object) $this->getHongkongData();


        // dd($data);
        if (isset($data->report['_last30DaysReport'][0])) {
            $lastPeriode = $data->report['_last30DaysReport'][0];
        }



        $keluaranSebelumnya =  $lastPeriode['keluaran'];
        $keluaranSebelumnya =  substr($keluaranSebelumnya, 2, 4);

        $tanggal = $lastPeriode['tanggal'];

        // override
        // $keluaranSebelumnya = 36;
        // $keluaranSebelumnya = 35;
        // $yesterday = '2020-04-29';

        $log = [];
        $log['angkaKeluar'] = $keluaranSebelumnya;
        $log['data'] = [];

        if (isset($_GET['kemarin'])) {
            $tebakanUser = TebakanUser::where('tanggal', $yesterday)->get();
        } else {
            $tebakanUser = TebakanUser::where('tanggal', $now)->get();
        }
        // $tebakanUser = TebakanUser::where('tanggal', $yesterday)->get();
        foreach ($tebakanUser as $key => $item) {
            // return $item;
            // return $item->tebakan;

            $xx = [
                'id' => $item->id,
                'user' => [
                    'id' => $item->id_user,
                    'name' => $item->userName,
                ],
                'tebakan' => $item->tebakan,

            ];
            if (Str::contains(implode(',', $item->tebakan), (string) $keluaranSebelumnya) === true) {

                $poin = 1;
            } else {

                $poin = 0;
            }
            $check  =  HasilTebak::where('id_tebakan', $item->id)->first();
            if ($check === null) {
                // return $item->tebakan;
                $inputHasil =  new HasilTebak(['id' => Str::uuid()]);
                $inputHasil->fill([
                    'id_tebakan' => $item->id,
                    'id_periode' => $item->id_periode,
                    'id_user' => $item->id_user,
                    'tanggal' => $now,
                    'poin' => $poin,
                    'angka_keluar' => $keluaranSebelumnya
                ]);
                $inputHasil->penilaian = $item->tebakan;
                $inputHasil->save();
            }



            // $inputHasil->save();

            array_push($log['data'], $xx);
        }

        return $log;



        // return $data;


        // $yesterda= ;


    }
}
