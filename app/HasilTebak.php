<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HasilTebak extends Model
{
    protected $table = 'lomba_hasil';
    protected $fillable = [
        'id',
        'id_tebakan',
        'id_periode',
        'id_user',
        'penilaian',
        'angka_keluar',
        'tanggal',
        'poin',

    ];
    protected $keyType = 'string';
    public $incrementing = false;
    protected $casts = [
        'id' => 'string',
        'penilaian' => 'json',

    ];

    // public function jackpot()
    // {
    //     return $this->hasOne('App\HasilTebak', 'foreign_key', 'local_key');
    // }
}
