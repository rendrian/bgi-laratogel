<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use App\TebakanUser;
use App\HasilTebak;

class hitunghasil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'togel:hitunghasil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hitung hasil setiap ham 2300 - 2400';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::today('Asia/Jakarta')->isoFormat('YYYY-MM-DD');
        $yesterday = Carbon::yesterday('Asia/Jakarta')->isoFormat('YYYY-MM-DD');

        // check last data =

        // $data = (object) json_decode(@file_get_contents(option('endPointKeluaranPerProvider') . 'hongkong'), true);
        // $data = (object) $this->getHongkongData();
        $data = (object) $this->getKeluaranData(option('lombaTogelProviderCode'));
        //  $data = $this->getKeluaranData(option('lombaTogelCountryCode'));
        //  print_r($data);
        // die;

        // dd($data);
        if (isset($data->report['_last30DaysReport'][0])) {
            $lastPeriode = $data->report['_last30DaysReport'][0];
        }




        $keluaranSebelumnya =  $lastPeriode['keluaran'];
        $keluaranSebelumnya =  substr($keluaranSebelumnya, 2, 4);

        $tanggal = $lastPeriode['tanggal'];

        // override
        // $keluaranSebelumnya = 36;
        // $keluaranSebelumnya = 35;
        // $yesterday = '2020-04-29';

        $log = [];
        $log['angkaKeluar'] = $keluaranSebelumnya;
        $log['data'] = [];

        $tebakanUser = TebakanUser::where('tanggal', $now)->get();
        // $tebakanUser = TebakanUser::where('tanggal', $yesterday)->get();
        foreach ($tebakanUser as $key => $item) {
            // return $item;
            // return $item->tebakan;

            $xx = [
                'id' => $item->id,
                'user' => [
                    'id' => $item->id_user,
                    'name' => $item->userName,
                ],
                'tebakan' => $item->tebakan,

            ];
            try {
                if (Str::contains(implode(',', (array) $item->tebakan), (string) $keluaranSebelumnya) === true) {

                    $poin = 1;
                } else {

                    $poin = 0;
                }
            } catch (\Throwable $th) {
                dd($item);
            }

            // dd($item);

            // dd($poin);
            $check  =  HasilTebak::where('id_tebakan', $item->id)->first();
            if ($check === null) {
                // return $item->tebakan;
                $inputHasil =  new HasilTebak(['id' => Str::uuid()]);
                $inputHasil->fill([
                    'id_tebakan' => $item->id,
                    'id_periode' => $item->id_periode,
                    'id_user' => $item->id_user,
                    'tanggal' => $now,
                    'poin' => $poin,
                    'angka_keluar' => $lastPeriode['keluaran']
                ]);

                $inputHasil->penilaian = $item->tebakan;
                $inputHasil->save();
            }



            // $inputHasil->save();

            array_push($log['data'], $xx);
        }

        echo json_encode($log, JSON_PRETTY_PRINT);
    }

    private function getHongkongData()
    {
        if (option('status') === 'local') {

            return json_decode(file_get_contents(public_path() . '/json/hongkong.json'), true);
        } else {
            return json_decode(@file_get_contents(option('endPointKeluaranPerProvider') . 'hongkong'), true);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }
    private function getKeluaranData($keluaran)
    {
        // return option('endPointKeluaranPerProvider') . $keluaran;
        if (option('status') === 'local') {

            return json_decode(file_get_contents(public_path() . '/json/hongkong.json'), true);
        } else {
            return json_decode(@file_get_contents(option('endPointKeluaranPerProvider') . $keluaran), true);


            // 'https://mafiahasil.top/api/data/all.json'
        }
    }
}
