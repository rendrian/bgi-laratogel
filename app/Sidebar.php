<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sidebar extends Model
{
    protected $table = 'sidebar';

    protected $fillable = [
        'name',  'title', 'html', 'position'
    ];
    protected $hidden = [
        'updated_at', 'created_at',
    ];
}
