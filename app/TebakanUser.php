<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class TebakanUser extends Model
{
    protected $table = 'lomba_tebakan';
    protected $fillable = ['id', 'id_user', 'id_periode', 'tebakan', 'void', 'keterangan', 'tanggal'];
    protected $keyType = 'string';
    public $incrementing = false;
    protected $casts = [
        'id' => 'string',
        'tebakan' => 'array'

    ];
    protected $appends = ['userName'];

    public function getuserNameAttribute()
    {
        // return $this->hasOne('App\User');

        $username =  User::find($this->attributes['id_user']);
        if ($username !== null) {
            return $username->name;
        } else {
            return 'NONAME';
        }

        // dd ($data);
    }
    public function getVoidAttribute()
    {
        // return $this->hasOne('App\User');

        return ($this->attributes['void'] === null ? false : true);

        // dd ($data);
    }


    public function statusTebakan()
    {
        return $this->hasOne('App\HasilTebak', 'id_tebakan', 'id');
    }

    public function getTebakanAttribute()
    {
        $array =  json_decode($this->attributes['tebakan']);
        return $array;
        for ($i = 0; $i <= 4; $i++) {
            if (!array_key_exists($i, $array)) {
                $array[$i] = '❌';
            }
        }
    }




    // public function user()
    // {
    //     return $this->hasMany('App\user');
    // }
    // public function user()
    // {
    //     return $this->belongsTo('App\User', 'id','id_user');
    // }

    public function user()
    {
        return $this->hasMany('App\User', 'id', 'id_user');
    }
}
