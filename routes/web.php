<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

use Faker\Factory as Faker;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::resource('tebakan', 'TebakanUserController');

// dukun togel

// dukun togel tebakan
Route::group(['prefix' => 'api-dukuntogel'], function () {
    Route::post('tebakan', 'DukunController@add');
    Route::get('tebakan', 'DukunController@status');
    Route::get('user', 'DukunController@user');
    Route::get('', 'DukunController@index');

    /**
     * Tambah  atau update tebakan
     * dibutuhkan :
     * id : uuid atau new
     * tanggal
     * tebakan = array('colokbebas'=>$colokbebas,'kombinasi'=>$kombinasi)
     */



    // Route::get('','DukunController@index');


});


Auth::routes();

Route::get('/flushcache', function () {
    Cache::flush();

});


Route::get('/logout', function () {
    Auth::logout();
});
Route::get('/dashboard/{any?}', 'HomeController@dashboard')->name('dashboard')->where('any', '.*');
Route::get('/' . option('sitemapPermalink'), 'SitemapController@index')->name('sitemap');


Route::get('/', 'PublicController@index')->name('home');
Route::get('/' . option('gotoProviderPermalinkPrefix') . '-{providername}' . option('gotoProviderPermalinkSuffix'), 'PublicController@today')->name('datakeluaran');
Route::get('/' . option('prediksiTogelPermalink'), 'PublicController@prediksiTogel')->name('prediksitogel');
Route::get('/' . option('gotoProviderPermalinkTomorrow') . '-{providername}-{hari}-{tanggal}', 'PublicController@tomorrow')->name('prediksi.tomorrow');

Route::get('/' . option('gotoRekomendasiPermalinkPage'), 'PublicController@viewRekomendasi')->name('rekomendasi');
if (strtolower(option('enableLombaTogel')) == 'true') {
    Route::get('/' .  option('tebakSkorPermalink'), 'TebakSkorController@index')->name('tebaknomor');
}
if (strtolower(option('enableDukunTogel')) == 'true') {
    Route::get('/' .  option('dukunTogelPermalink'), 'DukunTogel@index')->name('dukuntogel');
}


if (strtolower(option('enableLivedraw')) == 'true') {

    Route::group(['prefix' => option('livedrawPermalink')], function () {
        Route::get('/', 'LivedrawController@index')->name('livedraw-index');
    });
}







Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


// Route::get('/{any}', 'SpaController@index')->where('any', '.*');


// special page
Route::get('/' . option('permalinkShioTable'), function () {
    return PublicController::viewPageStatic('table-shio');
})->name('shio.table');
Route::get('/' . option('gotoProviderPermalinkPage') . '/{value}', 'PublicController@viewPage')->name('gotoPage');
